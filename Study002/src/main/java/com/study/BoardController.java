package com.study;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.study.service.BoardService;

/**
 * <pre>
 * 게시판 컨트롤러
 * </pre>
 * @fileName		: BoardController.java
 * @projectName		: Study002
 * @since			: 2017. 6. 23.
 * @author			: ybkim1
 */
@Controller
public class BoardController {

	@Autowired
	BoardService boardService;
	
	@RequestMapping("/board/boardList")
	public String boardList(@RequestParam Map map, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
	
		boardService.boardList(map, model);
		
		return "board";
	}
	
}
