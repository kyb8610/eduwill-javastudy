package com.study;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.study.service.IndexService;

/**
 * <pre>
 * 인덱스 컨트롤러
 * </pre>
 * @fileName		: IndexController.java
 * @projectName		: Study002
 * @since			: 2017. 6. 23.
 * @author			: ybkim1
 */
@Controller
public class IndexController {

	@Autowired
	IndexService indexService;
	
	@RequestMapping("/index.do")
	public String index(@RequestParam Map param, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		
		List list = indexService.selectListBoard(param);
		model.addAttribute("resultList", list);
		
		return "index";
		
	}
	
	@RequestMapping("/insertBoard.do")
	public String insertBoard(@RequestParam Map param, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		
		indexService.insertBoard(param);
		return "index";
		
	}
	
	@Test
	public void test1(){
		int a = 1+2;
		System.out.println(a);
	}
}
