package com.study.dao;

import java.util.List;
import java.util.Map;

public interface IndexDao {

	public List<Map<String, Object>> selectList(String queryId, Map<String, Object> param) throws Exception; 
	
	public Map<String, Object> select(String queryId, Map<String, Object> param) throws Exception;
	
	public int selectCount(String queryId, Map<String, Object> param) throws Exception;
	
	public int insert(String queryId, Map<String, Object> param) throws Exception;
	
	public int update(String queryId, Map<String, Object> param) throws Exception;
	
	public int delete(String queryId, Map<String, Object> param) throws Exception;
	
}
