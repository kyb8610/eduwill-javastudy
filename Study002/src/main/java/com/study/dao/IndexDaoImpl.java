package com.study.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository("indexDao")
public class IndexDaoImpl implements IndexDao {

	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;
	
	public List<Map<String, Object>> selectList(String queryId, Map<String, Object> param) throws Exception {
		return sqlSessionTemplate.selectList(queryId, param);
	}

	public Map<String, Object> select(String queryId, Map<String, Object> param) throws Exception {
		return sqlSessionTemplate.selectOne(queryId, param);
	}
	
	public int selectCount(String queryId, Map<String, Object> param) throws Exception {
		return (Integer)sqlSessionTemplate.selectOne(queryId, param);
	}

	public int insert(String queryId, Map<String, Object> param) throws Exception {
		return sqlSessionTemplate.insert(queryId, param);
	}

	public int update(String queryId, Map<String, Object> param) throws Exception {
		return sqlSessionTemplate.update(queryId, param);
	}

	public int delete(String queryId, Map<String, Object> param) throws Exception {
		return sqlSessionTemplate.delete(queryId, param);
	}

}
