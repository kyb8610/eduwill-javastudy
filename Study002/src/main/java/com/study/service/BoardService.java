package com.study.service;

import java.util.Map;

import org.springframework.ui.ModelMap;

public interface BoardService {

	public ModelMap boardList(Map map, ModelMap model) throws Exception;
	
}
