package com.study.service;

import java.util.List;
import java.util.Map;

public interface IndexService {

	public List selectListBoard(Map map) throws Exception;
	
	public Map selectBoard(Map map) throws Exception;
	
	public void insertBoard(Map map) throws Exception;
	
	public void deleteBoard(Map map) throws Exception;
	
	public int updateBoard(Map map) throws Exception;
	
}
