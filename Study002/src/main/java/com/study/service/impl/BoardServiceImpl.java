package com.study.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.study.dao.IndexDao;
import com.study.service.BoardService;

/**
 * <pre>
 * 게시판 서비스
 * </pre>
 * @fileName		: BoardServiceImpl.java
 * @projectName		: Study002
 * @since			: 2017. 6. 23.
 * @author			: ybkim1
 */
@Service("boardService")
public class BoardServiceImpl implements BoardService {

	@Autowired
	private IndexDao indexDao;
	
	/**
	 * <pre>
	 * 게시판 리스트를 불러온다.
	 * </pre>
	 * @methodName	: boardList
	 * @since			: 2017. 6. 23.
	 * @author			: ybkim1
	 * @param map
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public ModelMap boardList(Map map, ModelMap model) throws Exception {
		
		List list = indexDao.selectList("board.selectListBoard", map);
		model.addAttribute("list", list);
		return model;
		
	}

}
