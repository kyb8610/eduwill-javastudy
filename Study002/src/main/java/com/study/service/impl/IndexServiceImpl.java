package com.study.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.study.dao.IndexDao;
import com.study.service.IndexService;

@Service("indexService") 
public class IndexServiceImpl implements IndexService {

	@Autowired
	private IndexDao indexDao;
	
	@Transactional
	public List selectListBoard(Map map) throws Exception {
		
		int totalCount = indexDao.selectCount("board.selectCount", map);
		List list = new ArrayList();
		if ( totalCount > 0 ) {
			list = indexDao.selectList("board.selectListBoard", map);
		}
		return list;
	}

	public Map selectBoard(Map map) throws Exception {
		return indexDao.select("board.selectBoard", map);
	}

	public void insertBoard(Map map) throws Exception {
		indexDao.insert("board.insertBoard", map);
		indexDao.insert("board.insertBoardError", map);
		
		for ( int i=0; i<10; i++ ){
			System.out.println(i);
		}
	}

	public void deleteBoard(Map map) throws Exception {
		indexDao.delete("board.deleteBoard", map);
	}

	public int updateBoard(Map map) throws Exception {
		return indexDao.update("board.updateBoard", map);
	}
}
